#include "Catalog.h"

#include <iostream>
#include <algorithm>

using namespace std;

long long int Catalog::idGenerator = 0;

Catalog::Catalog(string &_name)
	: name(_name), id(idGenerator++)
{

}

Catalog::~Catalog()
{
	for(vector<Book*>::iterator it = books.begin(); it != books.end(); ++it)
	{
		delete *it;
	}
}

unsigned int Catalog::addBook()
{
	Book *book = new Book();
	books.push_back(book);

	cin >> *book;

	return books.size();
}

void Catalog::removeBook(Book &book)
{
	vector<Book*>::iterator it = find(books.begin(), books.end(), &book);
	books.erase(it);
	delete *it;
}

void Catalog::showBooks()
{
	cout << endl<< "---------- LISTA KSIĄŻEK ----------" << endl;
	for(unsigned int idx = 0; idx < books.size(); ++idx)
	{
		cout << idx << ". " << *books[idx] << endl;
	}
}

Book* & Catalog::operator[](unsigned int idx)
{
	return books[idx];
}

Book* & Catalog::operator()(unsigned int idx)
{
	return books[idx];
}

ostream& operator<<(ostream &os, const Catalog &catalog)
{
	os << catalog.name;
	return os;
}
