#include "Book.h"

#include <iostream>
#include <limits>

#include "Person.h"

long long int Book::idGenerator = 0;

Book::Book()
	: id(idGenerator++), author(NULL)
{

}

Book::~Book()
{
	if(author)
	{
		delete author;
	}
}

Book & Book::operator=(const Book &book)
{
	this->title = book.title;
	return *this;
}

istream& operator>>(istream &is, Book &book)
{
	cout << "Podaj tytuł książki: " << endl;
	cout << "> ";
    is.ignore(numeric_limits<streamsize>::max(), '\n');
	getline(is, book.title);

	string firstName;
	string lastName;

	cout << "Podaj imię autora: " << endl << "> ";
	cin >> firstName;
	cout << "Podaj nazwisko autora: " << endl << "> ";
	cin >> lastName;

	book.author = new Person(firstName, lastName);

	return is;
}

ostream& operator<<(ostream &os, const Book &book)
{
	os << book.title << ": " << *book.author;
	return os;
}
