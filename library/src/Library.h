#ifndef LIBRARY_H_
#define LIBRARY_H_

#include <ctime>
#include <string>
#include <vector>

#include "Catalog.h"

using namespace std;

class Book;
class User;

class Library {

public:
	class Rental;

	Library();
	~Library();

    unsigned int addBook();
    unsigned int addCatalog();
    unsigned int addUser();

    void removeBook();
    void removeUser();

    bool rentalExistsByBook(Book &book);
    bool rentalExistsByUser(User &user);

    Catalog & chooseCatalog();
    Book & chooseBook(Catalog &catalog);
    User & chooseUser();
    Rental & chooseRental();

    int validateInput(int min, int max, const char* prompt);

    void showBooks();
    void showCatalogs();
    void showUsers();

    void rentBook();
    void returnBook();
    void showRentals();
    void showArchiveRentals();

    void calculateFine(Rental& rental);

    Library& operator+=(Rental &rental);
    Library& operator-=(Rental &rental);

	class Rental
	{
    	friend class Library;
	private:
    	static long long int idGenerator;
    	long long int id;
		Book &book;
		User &user;
		unsigned int rentalDuration;
		time_t start;
		time_t end;

	public:
	    Rental(User &user, Book &book);

	    friend ostream& operator<<(ostream& os, Library::Rental &rental);
	};

private:
	void rentBookImpl(Rental& rental);
	void returnBookImpl(Rental& rental);

	vector<Catalog*> catalogs;
	vector<Rental*> rentals;
	vector<Rental*> archiveRentals;
	vector<User*> users;
};

#endif /* LIBRARY_H_ */
