#ifndef CATALOG_H_
#define CATALOG_H_

#include <ostream>
#include <string>
#include <vector>

#include "Book.h"

using namespace std;

class Catalog
{
public:
	Catalog(string &name);
	~Catalog();

	unsigned int addBook();
	void removeBook(Book &book);
	void showBooks();

	Book* & operator[](unsigned int idx);
	Book* & operator()(unsigned int idx);

	friend class Library;
	friend ostream& operator<<(ostream &os, const Catalog &catalog);

private:
	string name;
	vector<Book*> books;

	static long long int idGenerator;
	const long long int id;
};

#endif /* CATALOG_H_ */
