#include <iostream>

#include "Library.h"

using namespace std;

int main()
{
	Library library;

	int option;
	do
	{
		cout << endl << "---------- MENU ----------" << endl;
		cout << "0 - Koniec" << endl;
		cout << "1 - Dodaj katalog" << endl;
		cout << "2 - Wyświetl dostępne katalogi" << endl;
		cout << "3 - Dodaj książkę" << endl;
		cout << "4 - Usuń książkę" << endl;
		cout << "5 - Wyświetl dostępne książki" << endl;
		cout << "6 - Dodaj użytkownika" << endl;
		cout << "7 - Usuń użytkownika" << endl;
		cout << "8 - Wyświetl użytkowników" << endl;
		cout << "9 - Wypożycz książkę" << endl;
		cout << "10 - Wyswietl wypozyczone ksiazki" << endl;
		cout << "11 - Zwróć książkę" << endl;
		cout << "12 - Wyświetl archiwalne wypożyczenia" << endl;
		cout << "> ";

		cin >> option;

		switch(option)
		{
			case 1:
			{
				library.addCatalog();
				break;
			}
			case 2:
			{
				library.showCatalogs();
				break;
			}
			case 3:
			{
				library.addBook();
				break;
			}
			case 4:
			{
				library.removeBook();
				break;
			}
			case 5:
			{
				library.showBooks();
				break;
			}
			case 6:
			{
				library.addUser();
				break;
			}
			case 7:
			{
				library.removeUser();
				break;
			}
			case 8:
			{
				library.showUsers();
				break;
			}
			case 9:
			{
				library.rentBook();
				break;
			}
			case 10:
			{
				library.showRentals();
				break;
			}
			case 11:
			{
				library.returnBook();
				break;
			}
			case 12:
			{
				library.showArchiveRentals();
				break;
			}
		}

	} while(option);

	return 0;
}
