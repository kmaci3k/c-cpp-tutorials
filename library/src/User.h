#ifndef USER_H_
#define USER_H_

#include <string>

#include "Person.h"

using namespace std;

class User : public Person {
private:
	static long long int idGenerator;
	const long long int id;
	string login;

public:
	User(string &login, string &firstName, string &lastName);
	~User();

	friend class Library;
	friend ostream& operator<<(ostream &os, const User &user);
};

#endif /* USER_H_ */
