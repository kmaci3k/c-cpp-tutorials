#include "User.h"

#include <iostream>

long long int User::idGenerator = 0;

User::User(string &_login, string &_firstName, string &_lastName)
	: Person(_firstName, _lastName), id(User::idGenerator++), login(_login)
{

}

User::~User()
{

}

ostream& operator<<(ostream &os, const User &user)
{
	os << user.login << ": " << user.firstName << " " << user.lastName;
	return os;
}
