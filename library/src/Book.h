#ifndef BOOK_H_
#define BOOK_H_

#include <string>
#include <ostream>

using namespace std;

class Person;

class Book
{
private:
	static long long int idGenerator;
	const long long int id;
	string title;
	Person *author;

public:
	Book();
	~Book();

	Book & operator=(const Book &book);

	friend class Library;
	friend ostream& operator<<(ostream &os, const Book &book);
	friend istream& operator>>(istream &is, Book &book);
};

#endif /* BOOK_H_ */
