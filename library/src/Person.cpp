#include "Person.h"

#include <iostream>

Person::Person(string &_firstName, string &_lastName)
	: firstName(_firstName), lastName(_lastName)
{

}

Person::~Person()
{

}
ostream& operator<<(ostream &os, const Person &person)
{
	os << person.firstName << " " << person.lastName;
	return os;
}
