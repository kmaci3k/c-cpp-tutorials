#include "Library.h"

#include <iostream>
#include <algorithm>

#include "User.h"

using namespace std;

long long int Library::Rental::idGenerator = 0;

Library::Library()
{

}

Library::~Library()
{
	for(vector<Catalog*>::iterator it = catalogs.begin(); it != catalogs.end(); ++it)
	{
		delete *it;
	}

	for(vector<User*>::iterator it = users.begin(); it != users.end(); ++it)
	{
		delete *it;
	}
}

Library::Rental::Rental(User &_user, Book &_book)
	: book(_book), user(_user), rentalDuration(14)
{
	id = Library::Rental::idGenerator++;

	time_t now;
	time(&now);

	start = now;
	end = start + rentalDuration * 24 * 60 * 60; //day * 24h * 60min * 60s
}

int Library::validateInput(int min, int max, const char* prompt)
{
	int idx;
	do
	{
		cout << prompt << endl << "> ";
		cin >> idx;
	} while(idx < min || idx > max);

	return idx;
}

unsigned int Library::addBook()
{
	if(catalogs.empty())
	{
		cout << "Nie można dodać książki, ponieważ lista katalogów jest pusta..." << endl;
		return 0;
	}

	showCatalogs();
	int catalogIdx = validateInput(0, catalogs.size() - 1, "Wybierz katalog:");

	Catalog *catalog = catalogs[catalogIdx];
	return catalog->addBook();
}

unsigned int Library::addCatalog()
{
	string name;

	cout << "Podaj nazwe katalogu: " << endl;
	cout << "> ";
	cin >> name;

	Catalog *catalog = new Catalog(name);
	catalogs.push_back(catalog);

	return catalogs.size();
}

unsigned int Library::addUser()
{
	string login;
	string firstName;
	string lastName;

	cout << "Podaj login: " << endl << "> ";
	cin >> login;
	cout << "Podaj imię: " << endl << "> ";
	cin >> firstName;
	cout << "Podaj nazwisko: " << endl << "> ";
	cin >> lastName;

	User *user = new User(login, firstName, lastName);
	users.push_back(user);

	return users.size();
}

void Library::removeBook()
{
	if(catalogs.empty())
	{
		cout << "Nie można usunąć książki, ponieważ lista katalogów jest pusta..." << endl;
		return;
	}

	Catalog &catalog = chooseCatalog();
	Book &book = chooseBook(catalog);
	if(rentalExistsByBook(book))
	{
		cout << "Nie można usunąć książki, ponieważ jest/była wypożyczona..." << endl;
		return;
	}
	catalog.removeBook(book);
}

void Library::removeUser()
{
	if(users.empty())
	{
		cout << "Lista użytkowników jest pusta..." << endl;
		return;
	}

	User &user = chooseUser();
	if(rentalExistsByUser(user))
	{
		cout << "Nie można usunąć użytkownika, ponieważ wypożyczył książkę..." << endl;
		return;
	}

	vector<User*>::iterator it = find(users.begin(), users.end(), &user);
	users.erase(it);
	delete *it;
}

bool Library::rentalExistsByBook(Book &book)
{
	for(vector<Rental*>::iterator it = rentals.begin(); it != rentals.end(); ++it)
	{
		if((*it)->book.id == book.id)
		{
			return true;
		}
	}

	for(vector<Rental*>::iterator it = archiveRentals.begin(); it != archiveRentals.end(); ++it)
	{
		if((*it)->book.id == book.id)
		{
			return true;
		}
	}
	return false;
}

bool Library::rentalExistsByUser(User &user)
{
	for(vector<Rental*>::iterator it = rentals.begin(); it != rentals.end(); ++it)
	{
		if((*it)->user.id == user.id)
		{
			return true;
		}
	}

	for(vector<Rental*>::iterator it = archiveRentals.begin(); it != archiveRentals.end(); ++it)
	{
		if((*it)->user.id == user.id)
		{
			return true;
		}
	}
	return false;
}

Catalog & Library::chooseCatalog()
{
	showCatalogs();
	int catalogIdx = validateInput(0, catalogs.size() - 1, "Wybierz katalog:");
	return *catalogs[catalogIdx];
}
Book & Library::chooseBook(Catalog &catalog)
{
	catalog.showBooks();
	int bookIdx = validateInput(0, catalog.books.size() - 1 , "Wybierz książkę:");
	return *catalog[bookIdx];
}

User & Library::chooseUser()
{
	showUsers();
	int userIdx = validateInput(0, users.size() - 1, "Wybierz użytkownika:");
	return *users[userIdx];
}

Library::Rental & Library::chooseRental()
{
	showRentals();
	int rentalIdx = validateInput(0, rentals.size() - 1, "Wybierz książkę do zwrócenia:");
	return *rentals[rentalIdx];
}

void Library::showBooks()
{
	if(catalogs.empty())
	{
		cout << "Lista katalogów jest pusta..." << endl;
		return;
	}

	Catalog &catalog = chooseCatalog();
	catalog.showBooks();
}

void Library::showCatalogs()
{
	cout << endl<< "---------- LISTA KATALOGÓW ----------" << endl;
	for(unsigned int idx = 0; idx < catalogs.size(); ++idx)
	{
		cout << idx << ". " << catalogs[idx]->name << endl;
	}
}

void Library::showUsers()
{
	cout << endl<< "---------- LISTA UŻYTKOWNIKÓW ----------" << endl;
	for(unsigned int idx = 0; idx < users.size(); ++idx)
	{
		cout << idx << ". " << users[idx]->login << endl;
	}
}

void Library::rentBook()
{
	if(catalogs.empty())
	{
		cout << "Lista katalogów jest pusta..." << endl;
		return;
	}

	if(users.empty())
	{
		cout << "Lista użytkowników jest pusta..." << endl;
		return;
	}

	Catalog &catalog = chooseCatalog();
	Book &book = chooseBook(catalog);
	User &user = chooseUser();

	Rental *rental = (new Rental(user, book));
	*this += *rental;
}

void Library::rentBookImpl(Rental &rental)
{
	rentals.push_back(&rental);
}

void Library::returnBook()
{
	if(rentals.empty())
	{
		cout << "Lista wypożyczeń jest pusta..." << endl;
		return;
	}
	Rental &rental = chooseRental();
	*this -= rental;
}

void Library::returnBookImpl(Rental& rental)
{
	vector<Rental*>::iterator it = find(rentals.begin(), rentals.end(), &rental);
	rentals.erase(it);

	Rental* rentalToArchive = *it;
	calculateFine(*rentalToArchive);
	archiveRentals.push_back(rentalToArchive);
}

void Library::calculateFine(Rental& rental)
{
	time_t now;
	time(&now);

	long long int overdue = now - rental.end;
	if(overdue < 0)
	{
		cout << "Brak kary" << endl;
	} else
	{
		cout << "Kara: " << overdue / (24 * 60 * 60) << " dni po terminie" << endl; //24h * 60min * 60s
	}
}

Library& Library::operator+=(Rental &rental)
{
	rentals.push_back(&rental);
	return *this;
}
Library& Library::operator-=(Rental &rental)
{
	returnBookImpl(rental);
	return *this;
}

void Library::showRentals()
{
	cout << endl<< "---------- LISTA WYPOŻYCZEŃ ----------" << endl;
	for(unsigned int idx = 0; idx < rentals.size(); ++idx)
	{
		cout << idx << ". " << *rentals[idx] << endl;
	}
}

void Library::showArchiveRentals()
{
	cout << endl<< "---------- LISTA ARCHIWALNYCH WYPOŻYCZEŃ ----------" << endl;
	for(unsigned int idx = 0; idx < archiveRentals.size(); ++idx)
	{
		cout << idx << ". " << *archiveRentals[idx] << endl;
	}
}

ostream& operator<<(ostream& os, Library::Rental &rental)
{
	os << rental.user << endl;
	os << "ksiazka: " << rental.book << endl;
	os << "start: " << asctime(localtime(&rental.start));
	os << "end: " << asctime(localtime(&rental.end));

	return os;
}
