#ifndef PERSON_H_
#define PERSON_H_

#include <string>

using namespace std;

class User;

class Person {
public:
	Person(string &firstName, string &lastName);
	virtual ~Person();

	friend ostream& operator<<(ostream &os, const User &user);
	friend ostream& operator<<(ostream &os, const Person &person);
private:
	string firstName;
	string lastName;
};

#endif /* PERSON_H_ */
