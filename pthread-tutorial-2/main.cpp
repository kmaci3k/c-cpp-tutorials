#include <iostream>
#include <cstdlib>
#include <pthread.h>

using namespace std;

struct sum_runner_struct 
{
	long long limit;
	long long answer;
};

void* sum_runner(void* arg)
{
	struct sum_runner_struct *arg_struct = (sum_runner_struct *) arg;
	
	long long sum = 0;
	for(long long i = 0; i < arg_struct->limit; ++i)
	{
		sum += i;
	}
	arg_struct->answer = sum;
	
	pthread_exit(0);
}

int main(int argc, char *argv[])
{
	if(argc < 2)
	{
		cout << "Usage " << argv[0] << " <num 1> <num 2> ... <num-n>" << endl;
		exit(-1);
	}
	int num_args = argc - 1;
	
	// Thread Ids
	pthread_t tids[num_args];
	struct sum_runner_struct args[num_args];
	for(int i = 0; i < num_args; ++i)
	{
		// Create attributes
		pthread_attr_t attr;
		pthread_attr_init(&attr);
	
		args[i].limit = atoll(argv[i + 1]);
		pthread_create(&tids[i], &attr, sum_runner, &args[i]);		
	}
	
	// Do other stuff here
	
	// Wait until thread is done its work
	for(int i = 0; i < num_args; ++i)
	{
		pthread_join(tids[i], NULL);
		cout << "Thread " << i << ": sum is " << args[i].answer << endl;
	}
	
	
	return 0;
}
