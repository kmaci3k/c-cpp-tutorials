#ifndef ROBERTS_H_
#define ROBERTS_H_

#include <stdint.h>

class Bitmap;

class Roberts {
public:
	Roberts();
	~Roberts();

	void compute(Bitmap &inputImg, Bitmap &outputImg);

private:
	char convolution(char* upperLine, char* lowerLine, uint32_t idx, uint8_t numberOfClr);
};

#endif /* ROBERTS_H_ */
