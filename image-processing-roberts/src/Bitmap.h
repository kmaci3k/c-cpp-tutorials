#ifndef IMAGE_H
#define	IMAGE_H

#include <fstream>
#include <string>
#include <stdint.h>

using namespace std;

class Bitmap {

public:
	class FileHeader;
	class InfoHeader;

    Bitmap(string& filename);
    Bitmap(string &fn, Bitmap &bp);
    ~Bitmap();
    
    bool isLoaded();
    void readLineFromFile(char *buffer);
    void writeLineToFile(char *buffer);

    FileHeader getFileHeader();
    InfoHeader getInfoHeader();

    class FileHeader
    {
    public:
        uint16_t bfType;
        uint32_t bfSize;
        uint16_t bfReserved1;
        uint16_t bfReserved2;
        uint32_t bfOffBits;

    public:
        FileHeader(fstream &file);

        unsigned int readFile(fstream &file);
        void saveToFile(fstream &file);
    };
    
    class InfoHeader
    {
    public:
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXpelsPerMeter;
        uint32_t biYpelsPerMeter;
        uint32_t biCrlUses;
        uint32_t biCrlImportant;
        
    public:
        InfoHeader(fstream &file);

        unsigned int readFile(fstream &file);
        void saveToFile(fstream &file);
    };

private:
    string filename;
    fstream *file;

    FileHeader *fileHdr;
    InfoHeader *infoHdr;
};

#endif	/* IMAGE_H */

