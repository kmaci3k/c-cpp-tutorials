#include "Bitmap.h"

Bitmap::Bitmap(string &fn) : filename(fn), fileHdr(NULL), infoHdr(NULL)
{
    file = new fstream(filename.c_str(), std::fstream::in | std::fstream::binary);
    if(file->is_open())
    {
		fileHdr = new FileHeader(*file);
		infoHdr = new InfoHeader(*file);
    }
}

Bitmap::Bitmap(string &fn, Bitmap &bp) : filename(fn)
{
	file = new fstream(filename.c_str(), std::fstream::out | std::fstream::binary);
	if(bp.isLoaded())
	{
		fileHdr = new FileHeader(*bp.fileHdr);
		infoHdr = new InfoHeader(*bp.infoHdr);

		fileHdr->saveToFile(*file);
		infoHdr->saveToFile(*file);
	}
}

Bitmap::~Bitmap() 
{
	if(file->is_open())
	{
		delete fileHdr;
		delete infoHdr;
		file->close();
	}
    delete file;
}

bool Bitmap::isLoaded() 
{
    return file->is_open();
}

void Bitmap::readLineFromFile(char *buffer)
{
	for(uint32_t i = 0; i < infoHdr->biWidth * infoHdr->biBitCount / 8; ++i)
	{
		buffer[i] = file->get();
	}
}

void Bitmap::writeLineToFile(char *buffer)
{
	for(uint32_t i = 0; i < infoHdr->biWidth * infoHdr->biBitCount / 8; ++i)
	{
		file->put(buffer[i]);
	}
}

Bitmap::FileHeader Bitmap::getFileHeader()
{
	return *fileHdr;
}

Bitmap::InfoHeader Bitmap::getInfoHeader()
{
	return *infoHdr;
}

Bitmap::FileHeader::FileHeader(fstream &file)
{
	readFile(file);
}

unsigned int Bitmap::FileHeader::readFile(fstream &file)
{
	file.read(reinterpret_cast<char*>(&bfType), sizeof(bfType));
	file.read(reinterpret_cast<char*>(&bfSize), sizeof(bfSize));
	file.read(reinterpret_cast<char*>(&bfReserved1), sizeof(bfReserved1));
	file.read(reinterpret_cast<char*>(&bfReserved2), sizeof(bfReserved2));
	file.read(reinterpret_cast<char*>(&bfOffBits), sizeof(bfOffBits));

	return file.tellg();
}

void Bitmap::FileHeader::saveToFile(fstream &file)
{
	file.write(reinterpret_cast<char*>(&bfType), sizeof(bfType));
	file.write(reinterpret_cast<char*>(&bfSize), sizeof(bfSize));
	file.write(reinterpret_cast<char*>(&bfReserved1), sizeof(bfReserved1));
	file.write(reinterpret_cast<char*>(&bfReserved2), sizeof(bfReserved2));
	file.write(reinterpret_cast<char*>(&bfOffBits), sizeof(bfOffBits));
}

Bitmap::InfoHeader::InfoHeader(fstream &file)
{
	readFile(file);
}

unsigned int Bitmap::InfoHeader::readFile(fstream &file)
{
	file.read(reinterpret_cast<char*>(&biSize), sizeof(biSize));
	file.read(reinterpret_cast<char*>(&biWidth), sizeof(biWidth));
	file.read(reinterpret_cast<char*>(&biHeight), sizeof(biHeight));
	file.read(reinterpret_cast<char*>(&biPlanes), sizeof(biPlanes));
	file.read(reinterpret_cast<char*>(&biBitCount), sizeof(biBitCount));
	file.read(reinterpret_cast<char*>(&biCompression), sizeof(biCompression));
	file.read(reinterpret_cast<char*>(&biSizeImage), sizeof(biSizeImage));
	file.read(reinterpret_cast<char*>(&biXpelsPerMeter), sizeof(biXpelsPerMeter));
	file.read(reinterpret_cast<char*>(&biYpelsPerMeter), sizeof(biYpelsPerMeter));
	file.read(reinterpret_cast<char*>(&biCrlUses), sizeof(biCrlUses));
	file.read(reinterpret_cast<char*>(&biCrlImportant), sizeof(biCrlImportant));

	return file.tellg();
}

void Bitmap::InfoHeader::saveToFile(fstream &file)
{
	file.write(reinterpret_cast<char*>(&biSize), sizeof(biSize));
	file.write(reinterpret_cast<char*>(&biWidth), sizeof(biWidth));
	file.write(reinterpret_cast<char*>(&biHeight), sizeof(biHeight));
	file.write(reinterpret_cast<char*>(&biPlanes), sizeof(biPlanes));
	file.write(reinterpret_cast<char*>(&biBitCount), sizeof(biBitCount));
	file.write(reinterpret_cast<char*>(&biCompression), sizeof(biCompression));
	file.write(reinterpret_cast<char*>(&biSizeImage), sizeof(biSizeImage));
	file.write(reinterpret_cast<char*>(&biXpelsPerMeter), sizeof(biXpelsPerMeter));
	file.write(reinterpret_cast<char*>(&biYpelsPerMeter), sizeof(biYpelsPerMeter));
	file.write(reinterpret_cast<char*>(&biCrlUses), sizeof(biCrlUses));
	file.write(reinterpret_cast<char*>(&biCrlImportant), sizeof(biCrlImportant));
}
