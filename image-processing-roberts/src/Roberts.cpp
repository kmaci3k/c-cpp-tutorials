#include "Roberts.h"

#include <cmath>

#include "Bitmap.h"

Roberts::Roberts()
{

}

Roberts::~Roberts()
{

}

void Roberts::compute(Bitmap &inputImg, Bitmap &outputImg)
{
	Bitmap::InfoHeader infoHeader = inputImg.getInfoHeader();

	uint32_t lineLength = infoHeader.biWidth * infoHeader.biBitCount / 8;
	char *upperLine = new char[lineLength];
	char *lowerLine = new char[lineLength];
	char *outputLine = new char[lineLength]();

	inputImg.readLineFromFile(upperLine);

	outputImg.writeLineToFile(outputLine);
	for(uint32_t row = 0; row < infoHeader.biHeight - 1; ++row)
	{
		inputImg.readLineFromFile(lowerLine);

		for(uint32_t column = 1; column < infoHeader.biWidth - 1; ++column)
		{
			uint32_t blueIdx = column * (infoHeader.biBitCount / 8);
			outputLine[blueIdx] = convolution(upperLine, lowerLine, blueIdx, infoHeader.biBitCount / 8);

			uint32_t greenIdx = column * (infoHeader.biBitCount / 8) + 1;
			outputLine[greenIdx] = convolution(upperLine, lowerLine, greenIdx, infoHeader.biBitCount / 8);

			uint32_t redIdx = column * (infoHeader.biBitCount / 8) + 2;
			outputLine[redIdx] = convolution(upperLine, lowerLine, redIdx, infoHeader.biBitCount / 8);
		}

		char *tmpLine = upperLine;
		upperLine = lowerLine;
		lowerLine = tmpLine;

		outputImg.writeLineToFile(outputLine);
	}

	delete upperLine;
	delete lowerLine;
	delete outputLine;
}

char Roberts::convolution(char* upperLine, char* lowerLine, uint32_t idx, uint8_t numberOfClr)
{
	return (abs(upperLine[idx - numberOfClr] - lowerLine[idx])
			+
			abs(upperLine[idx] - lowerLine[idx - numberOfClr])
			) / 2;
}
