#include <iostream>
#include <fstream>
#include <cmath>

#include "Bitmap.h"
#include "Roberts.h"

using namespace std;

int main() {
    string inputImgFilename = "swinia_zawal1.bmp";
    Bitmap inputImg(inputImgFilename);
    
    if(!inputImg.isLoaded())
    {
        cout << "Could not open file: " << inputImgFilename << endl;
        return -1;
    }
    
    string outputImgFilename = "output.bmp";
    Bitmap outputImg(outputImgFilename, inputImg);

    Roberts roberts;
    roberts.compute(inputImg, outputImg);

    return 0;
}

