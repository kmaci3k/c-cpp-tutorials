#include <iostream>
#include <ncurses.h>
#include <unistd.h>

using namespace std;

#define DELAY 	50000

int main(int argc, char *argv[])
{	
	initscr();			// Initialize the window
	noecho();			// Don't echo any keypresses
	curs_set(FALSE);	// Don't display cursor
	
	int x = 0, y = 0;
	int max_y = 0, max_x = 0;
	int direction = 1;
	
	while(1)
	{
		// Call in order to update size of window
		getmaxyx(stdscr, max_y, max_x);
		
		clear();
		
		mvprintw(y, x, "o");
		refresh();
		
		int next_x = x + direction;

		if (next_x >= max_x || next_x < 0) 
		{
			direction*= -1;
		}
		else
		{
			x+= direction;
		}
		
		usleep(DELAY);
	}
	
	endwin(); 			// Restore normal terminal behavior
	 
	return 0;
}
