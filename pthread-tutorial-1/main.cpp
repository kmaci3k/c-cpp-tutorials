#include <iostream>
#include <cstdlib>
#include <pthread.h>

using namespace std;

long long sum = 0;

void* sum_runner(void* arg)
{
	long long *limit_ptr = (long long *) arg;
	long long limit = *limit_ptr;
	
	for(long long i = 0; i < limit; ++i)
	{
		sum += i;
	}
	
	pthread_exit(0);
}

int main(int argc, char *argv[])
{
	if(argc < 2)
	{
		cout << "Usage " << argv[0] << " <num>" << endl;
		exit(-1);
	}
	
	long long limit = atoll(argv[1]);
	
	// Thread Id
	pthread_t tid;
	
	// Create attributes
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	
	pthread_create(&tid, &attr, sum_runner, &limit);
	
	// Do other stuff here
	
	// Wait until thread is done its work
	pthread_join(tid, NULL);
	cout << "Sum is " << sum << endl;
	
	return 0;
}
