#include <iostream>
#include <vector>
#include <fstream>
#include <limits>

#include <cstdlib>
#include <cmath>

using namespace std;

#define PI 3.14159265

typedef struct coefficients_t
{
	string name;
	float value;
} coefficients_t;

typedef struct simulation_params_t
{
	float coef;
	float m;
	float d;
	float g;
	float x0;
	float y0;
	float v0;
	float alfa;
	float dt;
} simulation_params_t;

typedef struct simulation_results_t
{
	float y;
	float x;
	float v_y;
	float v_x;
	long long int iteration;
} simulation_results_t;

int open_file(ifstream **inFile, string &inFileName)
{
	*inFile = new ifstream();
	(*inFile)->open(inFileName.data(), ifstream::in);
	if(!(*inFile)->is_open())
	{
		cout << "Could not open input file: " << inFileName << endl;
		return -1;
	}
	return 0;
}

int read_coefficients_from_file(ifstream &inFile, vector<coefficients_t*> &coefficients)
{
	while(!inFile.eof())
	{
		string line;
		getline(inFile, line);

		if(!inFile.eof())
		{
			if(inFile.fail() || inFile.bad())
			{
				cout << "Could not read line from input file (too long line or missing delimiter ?)" << endl;
				return -1;
			}

			size_t delimiterPos = line.find_first_of('\t');
			if(delimiterPos == string::npos)
			{
				cout << "Following line " << line << " does not contain valid delimiter" << endl;
			}
			else
			{
				string name = line.substr(0, delimiterPos);
				float value = std::stof(line.substr(delimiterPos));

				coefficients_t *coef = new coefficients_t();
				coef->name = name;
				coef->value = value;

				coefficients.push_back(coef);
			}
		}
	}
	return 0;
}

coefficients_t * read_coef_from_user(vector<coefficients_t*> &coefficients)
{
	do
	{
		cout << "| Lp.\t| Osrodek     \t| Wartosc[kg/s] |" << endl;
		for(unsigned int i = 0; i < coefficients.size(); ++i)
		{
			coefficients_t *coef = coefficients[i];
			cout << "| " << i << ".\t| " << coef->name << "    \t| " << coef->value << "     \t|" << endl;
		}
		cout << "Wybierz srodowisko eksperymentu:" << endl << "> ";

		int input;
		cin >> input;
		if(cin.fail() || cin.bad())
		{
		    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		    cout << "Wpisany tekst nie jest liczba !!!" << endl << endl;
		}
		else if(input < 0 || input >= coefficients.size())
		{
			cout << "Podales liczbe z niewlasciwego zakresu !!!" << endl << endl;
		}
		else
		{
			return coefficients[input];
		}
	} while(1);
}

float read_param(string promptText)
{
	do
	{
		cout << promptText << endl << "> ";
		float input;
		cin >> input;
		if(cin.fail() || cin.bad())
		{
		    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		    cout << "Wpisany tekst nie jest liczba !!!" << endl << endl;
		}
		else
		{
			return input;
		}
	} while(1);
}

simulation_params_t read_simulation_params(vector<coefficients_t *> coefficients)
{
	simulation_params_t params;
	params.m = 1.4; // 1.4kg
	params.d = 0.1; // 10cm
	params.g = 9.81;
	params.coef = read_coef_from_user(coefficients)->value;
	params.x0 = 0;
	params.y0 = read_param("Podaj polozenie poczatkowe y0[m]: ");
	params.v0 = read_param("Podaj predkosc poczatkowa v0[m/s]: ");
	params.alfa = read_param("Podaj kat poczatkowy alfa[stopnie]: ");
	params.dt = read_param("Podaj krok calkowania dt[s]: ");

	return params;
}

simulation_results_t simulate(simulation_params_t params, ofstream &out)
{
	float v_x_t = params.v0 * cos(params.alfa * PI / 180);	// v_x_0
	float v_y_t = params.v0 * sin(params.alfa * PI / 180);	// v_y0
	float x_t = params.x0;
	float y_t = params.y0;

	float v_x_t_1 = 0;
	float v_y_t_1 = 0;
	float x_t_1 = 0;
	float y_t_1 = 0;

	long long int iteration = 0;
	do
	{
		// cout << iteration << ":\t" << x_t << "\t" << y_t << "\t" << v_x_t << "\t" << v_y_t << endl;

		v_x_t_1 = v_x_t - (params.coef / params.m) * v_x_t * params.dt;
		v_y_t_1 = v_y_t - ((params.coef / params.m) * v_y_t + params.g) * params.dt;

		x_t_1 = x_t + v_x_t * params.dt;
		y_t_1 = y_t + v_y_t * params.dt;

		out << iteration * params.dt << " " << x_t << " " << y_t << endl;

		v_x_t = v_x_t_1;
		v_y_t = v_y_t_1;

		x_t = x_t_1;
		y_t = y_t_1;

		++iteration;
	} while( (y_t - numeric_limits<float>::min()) > 0);

	simulation_results_t results;
	results.y = y_t;
	results.x = x_t;
	results.v_y = v_y_t;
	results.v_x = v_x_t;
	results.iteration = iteration;

	return results;
}

simulation_results_t experiment_one(simulation_params_t params, ofstream &out)
{
	simulation_results_t results_1 = simulate(params, out);
	cout << results_1.iteration << ":\t" << results_1.x << "\t" << results_1.y << "\t" << results_1.v_x << "\t" << results_1.v_y << endl;

	system ("gnuplot -persist skrypt.gp");

	return results_1;
}

simulation_results_t experiment_two(simulation_params_t params, ofstream &out)
{
	params.dt /= 2;
	simulation_results_t results_2 = simulate(params, out);
	cout << results_2.iteration << ":\t" << results_2.x << "\t" << results_2.y << "\t" << results_2.v_x << "\t" << results_2.v_y << endl;
	return results_2;
}

void experiment_three(simulation_params_t params, ofstream &out)
{
	float x_best = 0;
	float alfa_best = 0;
	for(unsigned int i = 0; i <= 900; ++i)
	{
		params.alfa = (float)i/10;
		simulation_results_t results_alfa = simulate(params, out);
		// cout << results_alfa.iteration << ":\t" << results_alfa.x << "\t" << results_alfa.y << "\t" << results_alfa.v_x << "\t" << results_alfa.v_y << endl;
		if(results_alfa.x > x_best)
		{
			x_best = results_alfa.x;
			alfa_best = params.alfa;
		}
	}
	cout << "Dla zadanych warunkow najwiekszy zasieg " << x_best << " osiaga sie dla kata " << alfa_best << " stopni." << endl;
}

int main(int argc, char** args)
{
	string inFileName("wspolczynniki.dat");

	if(argc > 1)
	{
		inFileName = args[1];
	}

	ifstream inFile;
	inFile.open(inFileName.data(), ifstream::in);
	if(!inFile.is_open())
	{
		cout << "Could not open input file: " << inFileName << endl;
		return -1;
	}

	ofstream nullFile("/dev/null", std::ofstream::out);
	ofstream graphFile("wynik.dat", std::ofstream::out);
	ofstream plotFile("skrypt.gp", std::ofstream::out);
	plotFile << "plot \"wynik.dat\" using 2:3 with lines" << endl;

	vector<coefficients_t*> coefficients;
	read_coefficients_from_file(inFile, coefficients);

	simulation_params_t params = read_simulation_params(coefficients);

	simulation_results_t results_1 = experiment_one(params, graphFile);
	simulation_results_t results_2 = experiment_two(params, nullFile);

	float dx = (100 * results_2.x / results_1.x) - 100;
	cout << "Zasieg rzutu wynosi: " << results_1.x << ". Dwukrotnie zmniejszenie kroku czasowego zmienia ten wynik o " << dx << "%." << endl;

	experiment_three(params, nullFile);

	inFile.close();
	nullFile.close();
	graphFile.close();
	plotFile.close();

	return 0;
}
