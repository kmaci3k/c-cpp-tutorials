### Przygotowanie do budowania aplikacji

```
sudo apt-get install build-essential
sudo apt-get install libncurses-dev
```

### Budowanie aplikacji

```
cd /your/download/path/swimming-pool
make swimming-pool
```

### Uruchomienie aplikacji

```
cd /your/download/path/swimming-pool/bin
./swimming-pool
```
