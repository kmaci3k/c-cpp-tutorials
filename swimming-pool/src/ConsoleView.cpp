#include "ConsoleView.h"
#include "model/Client.h"

#include <ncurses.h>
#include <cstring>
#include "model/Facilities.h"

ConsoleView::ConsoleView(Facilities &resources_arg, vector<Client *> &clients_count)
	: facilities(resources_arg), av_clients(clients_count)
{
	initscr();			// Initialize the window
	noecho();			// Don't echo any keypresses
	curs_set(FALSE);	// Don't display cursor
	nodelay(stdscr,TRUE);

	if(has_colors())
	{
		start_color();

	    /*
	     * Simple color assignment, often all we need.  Color pair 0 cannot
	     * be redefined.  This example uses the same value for the color
	     * pair as for the foreground color, though of course that is not
	     * necessary:
	     */
	    init_pair(1, COLOR_RED,     COLOR_BLACK);
	    init_pair(2, COLOR_GREEN,   COLOR_BLACK);
	    init_pair(3, COLOR_YELLOW,  COLOR_BLACK);
	    init_pair(4, COLOR_BLUE,    COLOR_BLACK);
	    init_pair(5, COLOR_CYAN,    COLOR_BLACK);
	    init_pair(6, COLOR_MAGENTA, COLOR_BLACK);
	    init_pair(7, COLOR_WHITE,   COLOR_BLACK);
	}
}

ConsoleView::~ConsoleView()
{
	endwin(); 			// Restore normal terminal behavior
}

void ConsoleView::draw()
{
	clear();

	// Draw resources
	draw_facilities();
	draw_clients();

	refresh();
}

void ConsoleView::draw_facilities()
{
	attrset(COLOR_PAIR(4));
	mvprintw(0, 0, "########################### RESOURCES ###########################");

	attrset(COLOR_PAIR(2));
	mvprintw(1, 0, "Tracks\t-> available: %u\tleft: %u", facilities.max_track(), facilities.available_track());
	mvprintw(2, 0, "Jacuzzi\t-> available: %u\tleft: %u", facilities.max_jacuzzi(), facilities.available_jacuzzi());
	mvprintw(3, 0, "Shower\t-> available: %u\tleft: %u", facilities.max_shower(), facilities.available_shower());
	mvprintw(4, 0, "Toilet\t-> available: %u\tleft: %u", facilities.max_toilet(), facilities.available_toilet());

	attrset(COLOR_PAIR(0));
}

void ConsoleView::draw_clients()
{
	unsigned int y_offset = 6;

	attrset(COLOR_PAIR(1));
	mvprintw(y_offset, 0, "############################ CLIENTS ############################");

	attrset(COLOR_PAIR(7));
	mvprintw(y_offset + 1, 0, "Number of clients:\t\t%u", av_clients.size());
	mvprintw(y_offset + 2, 0, "Number of active clients:\t%u", number_of_active_clients());

	attrset(COLOR_PAIR(2));

	for(unsigned int i = 0; i < av_clients.size(); ++i)
	{
		Client * client = av_clients[i];
		client_state_t state = client->state();
		facility_type_t current_facility = client->occupied_facility();
		facility_type_t next_facility = client->next_occupied_facility();
		unsigned int current_progress = client->current_progress();

		unsigned int buffer_size = 128;
		char buffer[buffer_size];
		translate_state(state, current_facility, next_facility, buffer, buffer_size);
		if(client->state() == IN_FACILITY)
		{
			unsigned int progress_offset = strlen(buffer) + 1;
			unsigned int progress_width = 10;
			buffer[progress_offset - 1] = '\t';
			buffer[progress_offset] = '|';

			for(unsigned int i = 0; i < 10; ++i)
			{
				if(i < current_progress)
				{
					buffer[progress_offset + 1 + i] = '#';
				}
				else
				{
					buffer[progress_offset + 1 + i] = ' ';
				}
			}
			buffer[progress_offset + progress_width + 1] = '|';
			buffer[progress_offset + progress_width + 2] = 0;

			//print progress in percent
			unsigned int percent_offset = strlen(buffer);
			buffer[percent_offset] = ' ';
			sprintf( (buffer + percent_offset + 1), "%u%%", current_progress * 10);
		}
		mvprintw(y_offset + 4 + i, 0, "Client %u\t-> Is %s", i, buffer);
	}
}

void ConsoleView::translate_state(client_state_t state, facility_type_t current_facility, facility_type_t next_facility, char* buffer, unsigned int buffer_size)
{
	unsigned int facility_buffer_size = 64;
	char facility_buffer[buffer_size];
	switch(state)
	{
	case FREE:
		strncpy(buffer, "FREE", buffer_size);
	case WAITING:
		translate_facility(next_facility, facility_buffer, facility_buffer_size);
		strncpy(buffer, "WAITING for\t", buffer_size);
		strncat(buffer, facility_buffer, buffer_size);
		break;
	case IN_FACILITY:
		translate_facility(current_facility, facility_buffer, facility_buffer_size);
		strncpy(buffer, "IN_FACILITY\t", buffer_size);
		strncat(buffer, facility_buffer, buffer_size);
		break;
	case OUT:
		strncpy(buffer, "OUT", buffer_size);
		break;
	default:
		strncat(buffer, "UNKNOWN STATE", buffer_size);
	}
}

void ConsoleView::translate_facility(facility_type_t facility, char* buffer, unsigned int buffer_size)
{
	switch(facility)
	{
	case EMPTY:
		strncpy(buffer, "EMPTY", buffer_size);
		break;
	case TRACK:
		strncpy(buffer, "track", buffer_size);
		break;
	case JACUZZI:
		strncpy(buffer, "jacuzzi", buffer_size);
		break;
	case SHOWER:
		strncpy(buffer, "shower", buffer_size);
		break;
	case TOILET:
		strncpy(buffer, "toilet", buffer_size);
		break;
	default:
		strncat(buffer, "UNKNOWN RESOURCE", buffer_size);
	}
}

unsigned int ConsoleView::number_of_active_clients()
{
	unsigned int active = 0;
	for(unsigned int i = 0; i < av_clients.size(); ++i)
	{
		if(av_clients[i]->state() != OUT)
		{
			++active;
		}
	}
	return active;
}
