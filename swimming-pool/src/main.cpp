#include <iostream>
#include <vector>
#include <cstdlib>
#include <unistd.h>
#include <ncurses.h>

#include "ConsoleView.h"
#include "model/Client.h"
#include "model/Facilities.h"

#define DELAY 	50000

#define CLIENT_COUNT		40
#define TRACK_COUNT			6
#define JACUZZI_COUNT		3
#define SHOWER_COUNT		4
#define TOILET_COUNT		1

using namespace std;

void clean_up(vector<Client *> clients)
{
	while(!clients.empty())
	{
		Client *client = clients.back();
		client->cancel();

		clients.pop_back();
		delete client;
	}
}

int main()
{
	vector<Client *> clients(CLIENT_COUNT);
	{
		Facilities facilities(TRACK_COUNT, JACUZZI_COUNT, SHOWER_COUNT, TOILET_COUNT);

		srand(time(NULL));
		client_args_t client_args[CLIENT_COUNT];
		for(unsigned int i = 0; i < CLIENT_COUNT; ++i)
		{
			Client *client = new Client(facilities);
			client_args_t client_arg = client_args[i];
			client_arg.seed = rand();
			client->start(&client_arg);

			clients[i] = client;
		}

		ConsoleView console(facilities, clients);

		while(1)
		{
			console.draw();
			usleep(DELAY);

			int c = getch();
			if(c != -1)
			{
				break;
			}
		}
	}

	clean_up(clients);

	return 0;
}
