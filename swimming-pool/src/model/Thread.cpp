#include "Thread.h"

Thread::Thread() : tid(0) {

}

Thread::~Thread() {

}

bool Thread::start(void *args)
{
	thread_args.thread = this;
	thread_args.args = args;

	return (pthread_create(&tid, NULL, job_entry, &thread_args) == 0);
}

void Thread::wait_for_exit()
{
	(void) pthread_join(tid, NULL);
}

void Thread::cancel()
{
	pthread_cancel(tid);
}

void * Thread::job_entry(void *this_arg)
{
	thread_args_t thread_args = * (thread_args_t *) this_arg;
	Thread *this_thread = thread_args.thread;
	this_thread->do_job(thread_args.args);
	return NULL;
}

pthread_t Thread::thread_id()
{
	return tid;
}
