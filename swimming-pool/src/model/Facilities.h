#ifndef FACILITIES_H_
#define FACILITIES_H_

#include <pthread.h>

class Client;

#define TRACK_PROBABILITY		40
#define JACUZZI_PROBABILITY		30
#define SHOWER_PROBABILITY		20
#define TOILET_PROBABILITY		10

typedef enum facility_type_t
{
	EMPTY,
	TRACK,
	JACUZZI,
	SHOWER,
	TOILET
} facility_type_t;

class Facilities {
private:
	unsigned int track;
	unsigned int jacuzzi;
	unsigned int shower;
	unsigned int toilet;

	unsigned int av_track;
	unsigned int av_jacuzzi;
	unsigned int av_shower;
	unsigned int av_toilet;

	pthread_mutex_t track_mutex;
	pthread_cond_t track_cond;

	pthread_mutex_t jacuzzi_mutex;
	pthread_cond_t jacuzzi_cond;

	pthread_mutex_t shower_mutex;
	pthread_cond_t shower_cond;

	pthread_mutex_t toilet_mutex;
	pthread_cond_t toilet_cond;

public:
	Facilities(unsigned int track_count, unsigned int jacuzzi_count, unsigned int shower_count, unsigned int toilet_count);
	virtual ~Facilities();

	unsigned int max_track();
	unsigned int max_jacuzzi();
	unsigned int max_shower();
	unsigned int max_toilet();

	unsigned int available_track();
	unsigned int available_jacuzzi();
	unsigned int available_shower();
	unsigned int available_toilet();

	void acquire_track();
	void release_track();
	void acquire_jacuzzi();
	void release_jacuzzi();
	void acquire_shower();
	void release_shower();
	void acquire_toilet();
	void release_toilet();

private:
	void acquire_facility(pthread_mutex_t *mutex, pthread_cond_t *cond, unsigned int *facility);
	void release_facility(pthread_mutex_t *mutex, pthread_cond_t *cond, unsigned int *facility);

};

#endif /* FACILITIES_H_ */
