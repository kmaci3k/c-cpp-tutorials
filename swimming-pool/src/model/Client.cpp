#include "Client.h"

#include <iostream>
#include <cstdlib>
#include <unistd.h>

using namespace std;

Client::Client(Facilities &facilities_arg)
	: seed(0), facilities(facilities_arg), current_state(FREE),
	  current_facility(EMPTY), next_facility(EMPTY), progress(0)
{

}

Client::~Client()
{

}

void Client::do_job(void *args)
{
	client_args_t client_args = * (client_args_t *) args;
	seed = client_args.seed;

	// choose resource if the client has just entered s-p
	if(current_facility == EMPTY && next_facility == EMPTY)
	{
		next_facility = choose_facility();
	}

	while(1)
	{
		next_facility = choose_facility();

		//acquire chosen facility
		about_to_acquire_facility();
		acquire_facility(next_facility);
		in_facility(next_facility);

		//leaving/releasing facility
		unsigned int progress_time = compute_facility_time(current_facility);
		do
		{
			usleep(progress_time);
		}while(stay_in_facility());
		usleep(LEAVING_TIME);
		release_facility();
		facility_released();

		//check if client wants to leave sp
		if(out())
		{
			leave_sp();
			return;
		}
	}
}

facility_type_t Client::choose_facility()
{
	unsigned int choice = make_choice();
	if(choice <= TRACK_PROBABILITY)
	{
		return TRACK;
	}
	else if(choice <= TRACK_PROBABILITY + JACUZZI_PROBABILITY)
	{
		return JACUZZI;
	}
	else if(choice <= TRACK_PROBABILITY + JACUZZI_PROBABILITY + SHOWER_PROBABILITY)
	{
		return SHOWER;
	}
	else
	{
		return TOILET;
	}
}

unsigned int Client::make_choice()
{
	return 100 * ((float)rand_r(&seed) / RAND_MAX);
}

void Client::about_to_acquire_facility()
{
	// mark client as waiting for facility
	current_state = WAITING;
}

void Client::acquire_facility(facility_type_t facility)
{
	switch(facility)
	{
	case TRACK:
		facilities.acquire_track();
		break;
	case JACUZZI:
		facilities.acquire_jacuzzi();
		break;
	case SHOWER:
		facilities.acquire_shower();
		break;
	case TOILET:
		facilities.acquire_toilet();
		break;
	default:
		break;
	}
}

void Client::in_facility(facility_type_t facility)
{
	current_state = IN_FACILITY;
	current_facility = facility;
	next_facility = EMPTY;
}

unsigned int Client::compute_facility_time(facility_type_t facility)
{
	switch(facility)
	{
	case TRACK:
		return TRACK_TIME;
	case JACUZZI:
		return JACUZZI_TIME;
	case SHOWER:
		return SHOWER_TIME;
	case TOILET:
		return TOILET_TIME;
		break;
	default:
		return 0;
	}
}

bool Client::stay_in_facility()
{
	++progress;
	return (progress < 10);
}

void Client::release_facility()
{
	switch(current_facility)
	{
	case TRACK:
		facilities.release_track();
		break;
	case JACUZZI:
		facilities.release_jacuzzi();
		break;
	case SHOWER:
		facilities.release_shower();
		break;
	case TOILET:
		facilities.release_toilet();
		break;
	default:
		break;
	}
}

void Client::facility_released()
{
	current_state = FREE;
	current_facility = EMPTY;
	progress = 0;
}

bool Client::out()
{
	unsigned int choice = make_choice();
	return !(choice <= STAY_IN_SP_PROBABILITY);
}

void Client::leave_sp()
{
	current_state = OUT;
	current_facility = EMPTY;
	next_facility = EMPTY;
}

client_state_t Client::state()
{
	return current_state;
}

unsigned int Client::current_progress()
{
	return progress;
}

facility_type_t Client::occupied_facility()
{
	return current_facility;
}

facility_type_t Client::next_occupied_facility()
{
	return next_facility;
}
