#ifndef MODEL_THREAD_H_
#define MODEL_THREAD_H_

#include <pthread.h>

class Thread;

typedef struct thread_args_t
{
	Thread *thread;
	void *args;
} thread_args_t;

class Thread {
public:
	Thread();
	virtual ~Thread();

	bool start(void *args);
	void wait_for_exit();
	void cancel();
	pthread_t thread_id();

protected:
	virtual void do_job(void *args) = 0;

private:
	static void * job_entry(void * this_arg);
	pthread_t tid;
	thread_args_t thread_args;
};

#endif /* MODEL_THREAD_H_ */
