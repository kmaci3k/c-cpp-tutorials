#include "Facilities.h"

Facilities::Facilities(unsigned int track_count, unsigned int jacuzzi_count, unsigned int shower_count, unsigned int toilet_count)
	: track(track_count), jacuzzi(jacuzzi_count), shower(shower_count), toilet(toilet_count),
	  av_track(track_count), av_jacuzzi(jacuzzi_count), av_shower(shower_count), av_toilet(toilet_count)
{
	pthread_mutex_init(&track_mutex, NULL);
	pthread_cond_init(&track_cond, NULL);

	pthread_mutex_init(&jacuzzi_mutex, NULL);
	pthread_cond_init(&jacuzzi_cond, NULL);

	pthread_mutex_init(&shower_mutex, NULL);
	pthread_cond_init(&shower_cond, NULL);

	pthread_mutex_init(&toilet_mutex, NULL);
	pthread_cond_init(&toilet_cond, NULL);
}

Facilities::~Facilities() {

}

unsigned int Facilities::max_track()
{
	return track;
}

unsigned int Facilities::max_jacuzzi()
{
	return jacuzzi;
}

unsigned int Facilities::max_shower()
{
	return shower;
}

unsigned int Facilities::max_toilet()
{
	return toilet;
}

unsigned int Facilities::available_track()
{
	return av_track;
}

unsigned int Facilities::available_jacuzzi()
{
	return av_jacuzzi;
}

unsigned int Facilities::available_shower()
{
	return av_shower;
}

unsigned int Facilities::available_toilet()
{
	return av_toilet;
}

void Facilities::acquire_track()
{
	acquire_facility(&track_mutex, &track_cond, &av_track);
}

void Facilities::release_track()
{
	release_facility(&track_mutex, &track_cond, &av_track);
}

void Facilities::acquire_jacuzzi()
{
	acquire_facility(&jacuzzi_mutex, &jacuzzi_cond, &av_jacuzzi);
}

void Facilities::release_jacuzzi()
{
	release_facility(&jacuzzi_mutex, &jacuzzi_cond, &av_jacuzzi);
}

void Facilities::acquire_shower()
{
	acquire_facility(&shower_mutex, &shower_cond, &av_shower);
}

void Facilities::release_shower()
{
	release_facility(&shower_mutex, &shower_cond, &av_shower);
}

void Facilities::acquire_toilet()
{
	acquire_facility(&toilet_mutex, &toilet_cond, &av_toilet);
}

void Facilities::release_toilet()
{
	release_facility(&toilet_mutex, &toilet_cond, &av_toilet);
}

void Facilities::acquire_facility(pthread_mutex_t *mutex, pthread_cond_t *cond, unsigned int *facility)
{
	pthread_mutex_lock(mutex);
	while(*facility <= 0)
	{
		pthread_cond_wait(cond, mutex);
	}
	--(*facility);
	pthread_mutex_unlock(mutex);
}

void Facilities::release_facility(pthread_mutex_t *mutex, pthread_cond_t *cond, unsigned int *facility)
{
	pthread_mutex_lock(mutex);
	++(*facility);
	pthread_cond_broadcast(cond);
	pthread_mutex_unlock(mutex);
}
