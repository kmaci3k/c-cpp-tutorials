#ifndef MODEL_CLIENT_H_
#define MODEL_CLIENT_H_

#include "Facilities.h"
#include "Thread.h"

#define STAY_IN_FACILITY_PROBABILITY	60
#define LEAVE_FACILITY_PROBABILITY		40

#define STAY_IN_SP_PROBABILITY			70
#define LEAVE_SP_PROBABILITY			30

#define LEAVING_TIME					250000
#define TRACK_TIME						1000000
#define JACUZZI_TIME					1500000
#define SHOWER_TIME						500000
#define TOILET_TIME						250000

typedef struct client_args_t
{
	unsigned int seed;
} client_args_t;

typedef enum client_state_t
{
	FREE,
	WAITING,
	IN_FACILITY,
	OUT
} client_state_t;

class Client : public Thread {
public:
	Client(Facilities &facilities_arg);
	virtual ~Client();

	client_state_t state();
	unsigned int current_progress();
	facility_type_t occupied_facility();
	facility_type_t next_occupied_facility();
protected:
	void do_job(void *args);

private:
	unsigned int make_choice();

	facility_type_t choose_facility();

	void about_to_acquire_facility();
	void acquire_facility(facility_type_t facility_type);
	void in_facility(facility_type_t facility);
	unsigned int compute_facility_time(facility_type_t facility);

	bool stay_in_facility();
	void release_facility();
	void facility_released();

	bool out();
	void leave_sp();

private:
	unsigned int seed;
	Facilities &facilities;
	volatile client_state_t current_state;
	volatile facility_type_t current_facility;
	volatile facility_type_t next_facility;
	volatile unsigned int progress;
};

#endif /* MODEL_CLIENT_H_ */
