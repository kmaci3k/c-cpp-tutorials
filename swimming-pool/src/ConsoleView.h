#ifndef CONSOLEVIEW_H_
#define CONSOLEVIEW_H_

#include <vector>
using namespace std;

#include "model/Client.h"

class Facilities;

class ConsoleView {
private:
	Facilities &facilities;
	vector<Client *> &av_clients;

public:
	ConsoleView(Facilities &resources_arg, vector<Client *> &clients_count);
	virtual ~ConsoleView();

	void draw();

private:
	void draw_facilities();
	void draw_clients();

	void translate_state(client_state_t state, facility_type_t current_facility, facility_type_t next_facility, char* buffer, unsigned int buffer_size);
	void translate_facility(facility_type_t resource, char* buffer, unsigned int buffer_size);
	unsigned int number_of_active_clients();
};

#endif /* CONSOLEVIEW_H_ */
