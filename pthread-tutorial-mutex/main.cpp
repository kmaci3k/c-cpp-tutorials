#include <iostream>
#include <pthread.h>

using namespace std;

#define NUM_LOOPS 20000000
long long sum = 0;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void* counting_thread(void * arg)
{
	int offset = * (int *) arg;
	
	for(long long i = 0; i < NUM_LOOPS; ++i)
	{
		pthread_mutex_lock(&mutex);
		
		sum += offset;
		
		pthread_mutex_unlock(&mutex);
	}
	
	pthread_exit(0);
}

int main(int argc, char *argv[])
{
	int offset1 = 1;
	pthread_t tid1;
	pthread_create(&tid1, NULL, counting_thread, &offset1);
	
	int offset2 = -1;
	pthread_t tid2;
	pthread_create(&tid2, NULL, counting_thread, &offset2);
	
	pthread_join(tid1, NULL);
	pthread_join(tid2, NULL);
	
	cout << "Sum: " << sum << endl;
	
	return 0;
}
